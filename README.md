# Resources and Feedback

This projects collects resources that might be useful to get started with GitLab for MLOps. 

## Documentation and resources ML/DS features

- Jupyter Notebook Rendering and Diffs
    - Docs: https://docs.gitlab.com/ee/user/project/repository/jupyter_notebooks/

- Model experiments: 
    - Docs: https://docs.gitlab.com/ee/user/project/ml/experiment_tracking/#machine-learning-model-experiments
    - Epic: https://gitlab.com/groups/gitlab-org/-/epics/9341
    - Repository with example: https://gitlab.com/gitlab-org/incubation-engineering/mlops/model_experiment_example
    - Demo: https://www.youtube.com/watch?v=uxweU4zT40c

- Model registry (early development, accepting suggestions):
    - Epic: https://gitlab.com/groups/gitlab-org/-/epics/9423
    - Update thread: https://gitlab.com/groups/gitlab-org/-/epics/9423#note_1437373082

## Contributing

If you want to contribute to MLOps features:
    - and you don't know where to start, you can pick from the list of [features that are seeking contributors](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Category%3AMLOps&label_name%5B%5D=Seeking%20community%20contributions&first_page_size=20). 
    - if you have your own idea, open up an issue and tag @eduardobonet so we can start discussing it and the best way to tackle it.

## General updates

GitLab is evolving the features of MLOps quickly. We post updates regularly on our youtube playlist: 

https://www.youtube.com/playlist?list=PL05JrBw4t0KpC6-JQy8lY4tNAZKXBaM_-

## Leaving feedback

If you have a question, concern, request or idea, and you prefer not to post on the main issue board for that feature and keep it 
private, please create an issue in the repository, and assign to @eduardobonet.
